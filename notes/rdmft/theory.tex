\documentclass[%
%  reprint,
% superscriptaddress,
%groupedaddress,
%unsortedaddress,
%runinaddress,
%frontmatterverbose, 
preprint,
%preprintnumbers,
%nofootinbib,
%nobibnotes,
%bibnotes,
 amsmath,amssymb,
 aps,
 pra,
%prb,
%rmp,
%prstab,
%prstper,
%floatfix,
]{revtex4-2}
\usepackage{xcolor}
\usepackage{geometry}
\usepackage{graphicx}% Include figure files
\usepackage{braket}% Dirac notations
\usepackage{enumitem}

\def\njust{Institute of Ultrafast Optical Physics, Department of Applied Physics, Nanjing University of Science and Technology, Nanjing 210094, China}
\input{macro}

\begin{document}


\title{Implementation of reduced density matrix functional theory in Qunatum Espresso}
\author{Kai Luo}

\affiliation{\njust}%

% \affiliation{Nanjing University of Science and Technology}
\email{kluo@njust.edu.cn}
\date{Created on Aug 23, 2022, last updated on \today}

\maketitle

\section{Introduction}
The effort of finding the exact solution to the quantum many-body equation (e.g. Schr\"odinger's equation for non-relativistic treatment) 
is exponentially increasing with the number of electrons. Alternative methods have to be used while keeping the notion that 
solving problem in an affordable amount of time often bears a tolerated loss of accuracy. Tremendous success has been witnessed for the 
development of density functional theory (DFT) with the graceful balance between efficiency and accuracy. However, DFT with 
the state-of-art exchange-correlation (xc) functionals has its own limitation
in treating systems with strong correlations (e.g. transition metals). Often, hybrid functionals that density functionals augmented by 
a fraction of the Hartree-Fock exchange offer some remedies. More expensive density based methods or wave-function methods are possible 
for small molecules. Here, we are investigating the density-matrix based methods in general. The particular focus here is the 
reduced density matrix functional theory for solids where the energy is a functional of the one-body reduced density matrix (1-rdm). 

\section{Theory}
The Hamiltonian for $N$ electrons in a given field $V_\ext$, is 
\begin{equation}
    H_N=\sum_{i=1}^{N}\left[\frac{\hbar^{2}}{2 m} \nabla_{i}^{2}+V_\ext \left(\br_{i}\right)\right]+\sum_{i < j }^{N} U\left(\br_{i}-\br_{j}\right)
\end{equation}
where interaction between electrons is a two-body term $U(\br_i -\br_j)$. From now on,  Hartree atomic units with $\hbar=m=e^2=1$ are used.
The ground state energy is delivered from the Rayleigh-Ritz variational principle by minizing the expectation of the Hamiltonian 
\begin{equation}
    E_0 [\Psi] = \min_{\Psi} \braket{\Psi|H_N|\Psi}  
\end{equation}
where the Dirac notation is used 
\begin{eqnarray}
    \braket{f|O|g} \equiv \int f^{*}(\br_1, \br_2 \cdots, \br_N)  O g(\br_1, \br_2 \cdots, \br_N)  d^{3} r_1 d^{3} r_2 \cdots d^{3} r_N   \,.
\end{eqnarray}

We may now define the reduced density matrices that frequently appears in the literature:
\begin{enumerate}[label=(\arabic*)]
    \item the two-body density matrix (2-rdm):  
    \begin{equation}
        \label{eq:2rdm}
        \begin{array}{c}
        \Gamma\left(\br_1, \br_2 \mid \br_1', \br_2'\right)=N(N-1) \int \Psi^{*}\left(\br_1, \br_2, \br_3, \cdots, \br_N\right) \\
        \quad \times \Psi\left(\br_1', \br_2', \br_3, \cdots, \br_N\right) d^{3} r_3 \cdots d^{3} r_N \,,
        \end{array}
    \end{equation} 
    \item the one-body density matrix (1-rdm)
    \begin{equation}
        \label{eq:1rdm}
        \begin{aligned}
        \gamma\left(\br \mid \br'\right)=& N \int \Psi^{*}\left(\br, \br_2, \cdots, \br_N\right) \\
        & \times \Psi\left(\br', \br_2, \cdots, \br_N\right) d^{3} r_2 \cdots d^{3} r_N \,,
        \end{aligned}
        \end{equation}
\end{enumerate}
Diagonalization of 1-rdm produces a set of natural orbitals $\phi_i$ and occupation numbers $\eta_i$, 
\begin{equation}
    \hat{ \gamma }\phi_i (\br) = \eta_i \phi(\br)
\end{equation}
where $\hat{\gamma}$ acting on the function $\phi_i$ is understood as an integral operator, $\hat{\gamma} f(\br) = \int \gamma(\br \mid \br') f(\br') d^3 r' $.
Hence we have the spectral representation of $\gamma$
\begin{equation}
    \label{eq:spetral_representation}
    \gamma(\br \mid \br') = \sum_i \eta_i \phi_i(\br) \phi_i^*(\br') \,.
\end{equation} 
And quantities of interest that are measurable are 
\begin{enumerate}


    \item the two-particle correlation function 
    \begin{equation}
        \rho(\br_1, \br_2) = N (N - 1) \int \left| \Psi(\br_1, \br_2, \cdots, \br_N)\right| ^2 d^{3} r_3\cdots d^{3} r_N \,,
    \end{equation}

    \item the momentum density 
    \begin{equation}
        \begin{array}{l}
        \rho(\bk)=\frac{N}{(2 \pi)^{3}} \int e^{i \bk \cdot\left(\br-\br'\right)} \Psi^{*}\left(\br, \br_2, \cdots, \br_N\right) \\
        \quad \times \Psi\left(\br', \br_2, \cdots, \br_N\right) d^{3} r d^{3} r' d^{3} r_2 \cdots d^{3} r_N \,,
        \end{array}
        \end{equation}

    \item the density 
    \begin{equation}
        n(\br) = \gamma(\br \mid \br) =  N \int\left|\Psi\left(\br, \br_2, \cdots, \br_N\right)\right|^{2} d^{3} r_2 \cdots d^{3} r_N\,.
    \end{equation}    
\end{enumerate}

One can immediately see 
\begin{equation}
    \rho(\bk)=\frac{1}{(2 \pi)^{3}} \int e^{i \bk \cdot \br} \gamma\left(\br \mid \br'\right) e^{-i \bk \cdot \br'} d^{3} r d^{3} r'
\end{equation}
and
\begin{equation}
    \rho(\br_1, \br_2)= \Gamma(\br_1, \br_2 \mid \br_1, \br_2)
\end{equation}
and
\begin{equation}
    \gamma\left(\br \mid \br'\right)=\frac{1}{N-1} \int \Gamma\left(\br, \br_{2} \mid \br', \br_{2}\right) d^{3} r_{2}
\end{equation}

Normalization for 2-rdm (trace) is 
\begin{equation}
    \tr \ \Gamma = \int \Gamma(\br_1, \br_2 \mid \br_1, \br_2) d^3 r_1 d^3 r_2 = N (N-1)\,.
\end{equation}
These objects of interest originate from the fact that the interactions in the Hamiltonian involves either one-body or two-body terms. The 
energy thus can be expressed in terms of these objects. Namely, 

\begin{eqnarray}
    E[\Psi] = \half \int k^2 \rho(\bk) d^3 k + \int V(\br) n(\br) d^3r + \half \int U(\br_1 - \br_2) \rho(\br_1, \br_2)
\end{eqnarray}

One can also write the kinetic energy part as 
\begin{eqnarray}
    -\half \int \lim_{\br \to \br'} \nabla^2 \gamma(\br, \br') d^3 r' \,.
\end{eqnarray}
Following the usual decomposition of Kohn exchange energy, the interaction energy is split into the Hartree energy and exchange-correlation energy
\begin{eqnarray}
    \half \int \frac{n(\br) n(\br')}{|\br - \br'|} d^3r d^3r' + E_\xc[\gamma] \,,
\end{eqnarray}
where $E_\xc$ is a functional of 1-rdm to be approximated in the RDMFT. One has to note that at $T=0$ K, there is the no Kohn-Sham systems due to the 
non-idempotent density matrix. For finite-temperature, one can construct a Kohn-Sham system.

\subsection{Functionals}
The simplest xc functionals in RDMFT is the Hartree-Fock where 
\begin{equation}
    E_\xc[\gamma] = -\half \int \frac{|\gamma(\br,\br') |^2}{|\br-\br'|} d^3r d^3r' \,.
\end{equation}
Quoted from Ref. \cite{Sharma2008}, ``The HF functional satisfies the exact condition of the xc hole integrating
to minus one; however it does not satisfy the condition of
convexity, which is required by the exact functional."

The surge of RDMFT starts with the M\"uller functional where 
\begin{equation}
    E_\xc[\gamma] = -\half \int \frac{\gamma^{*p}
(\br,\br') \gamma^{1-p}(\br,\br')}{|\br-\br'|} d^3r d^3r' \,.
\end{equation}
with $p=\half$ and the spectral representation $\gamma^{\alpha} = \sum_i \eta_i^{\alpha} \phi_i(\br) \phi_i^{*}(\br')$.

Compactly, we can write the xc energy as  
\begin{equation}
    E_\xc[\gamma] = -\half \sum_{j,k=1}^{\infty} f(\eta_j, \eta_k)  \iint d^3r d^3r'  \frac{\phi_j(\br) \phi_j^{*}(\br') \phi_k^{*}(\br) \phi_k(\br')}{|\br-\br'|}
\end{equation}
Using this form, we note that Hartree-Fock functional has $f(\eta_j, \eta_k) = \eta_j \eta_k$, and 
the M\"uller functional has $f(\eta_j, \eta_k) = \sqrt{\eta_j \eta_k}$. Another progress is the 
Goedecker-Umrigar functional where $(\eta_j, \eta_k) = \sqrt{\eta_j \eta_k} (1-\delta_{jk}) + \eta_j^2 \delta_{jk}$.
Another zoo is the BBC functional family. 

\subsection{Periodic properties}
For a periodic potential where 
\begin{equation}
    \label{eq:periodic_pot}
    V(\br_1 + \bT , \br_2, \cdots, \br_N) =  V(\br_1 , \br_2, \cdots, \br_N)
\end{equation}
with translation $\bT$ in one of the coordinates. It follows that the Hamiltonian commutes with the translation operator $\bT$
\begin{equation}
    \hat{H} \hat{\bT} = \hat{\bT} \hat{H} \,.
\end{equation}
which leads to the Bloch theorem for the many-body wavefunction 
\begin{equation}
    \Psi_{\bK}(\br_1+ \bT, \br_2, \cdots, \br_N) = e^{i\bK \cdot \bT } \Psi_{\bK}(\br_1, \br_2, \cdots, \br_N) \,.
\end{equation}
Here $\bK$ is the crystal momentum.
The wavefunction can be written
\begin{equation}
    \Psi_{\bK}(\br_1, \br_2, \cdots, \br_N) = e^{i\bK\cdot (\br_1+\br_2+\cdots \br_N) }u_{\bK} (\br_1, \br_2, \cdots, \br_N)
\end{equation}
with periodic function $u_{\bK} (\br_1 + \bT, \br_2, \cdots, \br_N) = u_{\bK} (\br_1, \br_2, \cdots, \br_N)$. Similarly, we have 
\begin{equation}
    \Psi_{\bK}(\br_1, \br_2+ \bT, \cdots, \br_N) = e^{i\bK \cdot \bT } \Psi_{\bK}(\br_1, \br_2, \cdots, \br_N) \,.
\end{equation}
From Eq. \ref{eq:1rdm}, one can see 
\begin{equation}
    \label{eq:claimed_identity}
    \gamma(\br + \bT\mid \br'+\bT) = \gamma(\br\mid\br') \,.
\end{equation}
It leads to the identity
\begin{equation}
    \label{eq:spectral_identity}
    \sum_i \eta_i \phi_i (\br+\bT) \phi_i^*(\br' + \bT) = \sum_i \eta_i \phi_i(\br) \phi_i^*(\br')
\end{equation}
Multiply both sides with $\phi_j(\br')$ and integrate, we have 
\begin{equation}
    \label{eq:orbintegration}
    \sum_i \eta_i  \phi_i (\br+\bT)  \int d^3r' \phi_j(\br')  \phi_i^*(\br' + \bT) =   \int d^3r' \phi_j(\br') \sum_i \eta_i \phi_i(\br) \phi_i^*(\br') = \eta_j \phi_j(\br) 
\end{equation}
This holds true for any $\br$, which requires $\int d^3r' \phi_j(\br')  \phi_i^*(\br' + \bT)$ is proportional to $\delta_{ij}$. 
We know the orthonormal conditions for the natural orbitals $\phi_i$, and we are convinced that 
\begin{equation}
    \label{eq:norb_norm}
    \phi_i(\br + \bT) = \alpha \phi_i(\br) \,,
\end{equation}
with undetermined coefficient $\alpha$. Therefore, inserting Eq. \ref{eq:norb_norm} into Eq. \ref{eq:orbintegration}, we have $|\alpha| = 1$.
In order to assure the Bloch theorem for the natural orbital, we need more than Eq. \ref{eq:spectral_identity}. 
Let's look at 
\begin{eqnarray}
    \gamma(\br+\bT\mid \br') = e^{-i \bK \cdot \bT} \gamma(\br \mid \br') \nonumber
    \\
    \sum_i \eta_i \phi_i (\br+\bT) \phi_i^*(\br') =  e^{-i \bK \cdot \bT} \sum_i \eta_i \phi_i(\br) \phi_i^*(\br')
\end{eqnarray}
Hence, it can be seen the Bloch theorem reads
\begin{equation}
    \phi_i (\br+\bT) =  e^{-i \bK \cdot \bT} \phi_i(\br) \,.
\end{equation}
Therefore, it is not right  that it rises from Eq. \ref{eq:claimed_identity} as claimed by Sharma et al. \cite{Sharma2008}.

\section{Minimization}
\label{sec:minimization}
\subsection{Planewave basis}
For periodic systems, it is convenient to use planewaves as a basis to expand the orbitals. A planewave basis set for states of wave 
vector $\bk$ is defined as
\begin{equation}
    \langle\br \mid \bk+\mathbf{G}\rangle=\frac{1}{\sqrt{N V_c}} e^{i(\bk+\mathbf{G}) \cdot \br}, \quad \frac{\hbar^2}{2 m}|\bk+\mathbf{G}|^2 \leq E_{c u t}
\end{equation}
with unit cell volume, $V_c$, crystal volume $NV_c$, energy cutoff $E_{cut}$. We may expand a Bloch orbital $\psi_{n \bk}(\br)$ using the planewave basis (presumably complete)

\begin{equation}
    \psi_{n \bk}(\br)=\frac{1}{(N V_c)^{1 / 2}} \sum_{\mathbf{G}} C_{\mathbf{G} n \bk} e^{i(\mathbf{G}+\bk) \br}
\end{equation}
where $C_{\mathbf{G} n \bk}$ is the expansion coefficient.
Transformation by means of FFT between “real” space and “reciprocal” space is possible,
\begin{equation}
    C_{\mathbf{r} n \mathbf{k}}=\sum_{\mathbf{G}} C_{\mathbf{G} n \mathbf{k}} e^{i \mathbf{G r}} \stackrel{\mathrm{FFT}}{\longleftrightarrow} C_{\mathbf{G} n \mathbf{k}}=\frac{1}{N_{\mathrm{FFT}}} \sum_{\mathbf{r}} C_{\mathbf{r} n \mathbf{k}} e^{-i \mathbf{G r}}
\end{equation}

\subsection*{Matrix representation}
The free-energy in the RDMFT (T>0) is a functional of the 1rdm. Often, the 1rdm is written in terms of the natural orbitals and 
natural occupation numbers using the spectral representation. Approximation is adopted in the xc functional which might not be 
a functional of 1rdm only. More generally, xc energy is a functional of the natural orbitals and natural occupation numbers. People
call it natural orbital functional theory in the quantum chemistry community. The free energy  $\cal F[\gamma]$ is 
\begin{equation}
    {\cal F} [\gamma] = \Omega_k[\gamma]-\frac{S_\s[\gamma]}{\beta} + V[\gamma] + \Omega_{\Hxc}[\gamma]  \,.
\end{equation}
Each term carries a $\gamma$ to denote the functional dependence. 
Functionals of kinetic energy, external potential (nonlocal in general), entropy are
\begin{equation}
\begin{array}{c}
\Omega_k[\gamma]=\int d \br' \lim _{\br \rightarrow \br'}\left(-\frac{\nabla^2}{2}\right) \gamma\left(\br, \br'\right), \\
V[\gamma]=\int d \br d \br' v\left(\br, \br'\right) \gamma\left(\br', \br\right), \\
S_{\s}[\gamma]=-\sum_i\left[\eta_i \ln \eta_i+\left(1-\eta_i\right) \ln \left(1-\eta_i\right)\right], \\
% \Omega_{\Hxc}[\gamma]=  \\
% \Omega_{\x}[\gamma]=-\frac{1}{2} \int d \br d \br' w\left(\br, \br'\right) \gamma\left(\br, \br'\right) \gamma\left(\br', \br\right)
\end{array}
\end{equation}
The Hartree-exchange-correlation energy is unspecified yet, which we shall discuss it in detail later.

If we express the natural orbitals in the PW basis $b_{\alpha}$, using the vector notation 
\begin{equation}
    \phi_i = \sum_{\alpha}^{N_{basis}} X_{i\alpha} b_{\alpha}
\end{equation}
where $i=1, 2, \cdots, n$, $n$ is the number of bands involved. The basis has dimension of $m$, which usually is much larger than $n$, $m\gg n$.
For example, the input for NiO in the note with energy cutoff $E_{cut}$ has $m=1837$ G-vectors, while $n$ is only $8$ (could specify it with keyword {\textsf{nbnd}}).
We denote the orbitals by $\bX$ (a matrix of $m\times n$ elements) and the occupation numbers by $\bf$ (a vector of $n$ elements). Following the notation in Baarman et al.\cite{Baarman2016}, 
\begin{equation}
    \begin{array}{l}
    \mathbf{X} \in \mathcal{M}=\left\{\mathbf{X} \in \mathbb{R}^{m \times n} \mid \mathbf{X}^T \mathbf{X}=\mathbf{I}\right\} \\
    \mathbf{f} \in Q=\left\{\mathbf{f} \in[0,1]^n \mid \sum_{i=1}^n f_i=n_e\right\}
    \end{array}
\end{equation}
where $n_e$ is the number of electrons in the system so that $n_e \leq  n$. The orthogonality constraint on $\bX$ means that $\cal M$ is a 
Stiefel manifold. At $\bX$, the tangent space 
\begin{equation}
    T_{\mathbf{X}} \mathcal{M}=\left\{\mathbf{Y}=\mathbf{X B}+\mathbf{Z} \mid \mathbf{B}^T=-\mathbf{B} \text { and } \mathbf{Z}^T \mathbf{X}=\mathbf{0}\right\}
\end{equation}
where $\mathbf{Y}, \mathbf{Z} \in \mathbb{R}^{m \times n}$ and $\mathbf{B} \in \mathbb{R}^{n \times n}$.

Given an arbitrary matrix $\mathbf{V} \in \mathbb{R}^{m \times n}$ its orthogonal projection onto $T_{\mathbf{X}} \mathcal{M}$ is
\begin{equation}
    \mathbf{Y}=\mathbf{P}_{\mathbf{X}}(\mathbf{V})=\left(\mathbf{I}-\frac{1}{2} \mathbf{X} \mathbf{X}^T\right) \mathbf{V}-\frac{1}{2} \mathbf{X} \mathbf{V}^T \mathbf{X}
\end{equation}
For any vector $\bV$, 


The existence of a Kohn-Sham system in finite-temperature RDMFT \cite{Baldsiefen2015} thus allows us to write the grand potential
\begin{equation}
    \Omega[\gamma]=F[\gamma]+V[\gamma]-\mu N[\gamma] = {\cal F}[\gamma] - \mu N[\gamma]
    \end{equation}
where the universal functional is 
\begin{equation}
    F[\gamma]=\Omega_k[\gamma]-\frac{S_\s[\gamma]}{\beta}+\Omega_{\H}[\gamma]+\Omega_{\x}[\gamma] + \Omega_{\c}[\gamma]
\end{equation}
Functionals of kinetic energy, external potential,
particle number, KS entropy, Hartree energy, exchange energy are
\begin{equation}
\begin{array}{c}
\Omega_k[\gamma]=\int d \br' \lim _{\br \rightarrow \br'}\left(-\frac{\nabla^2}{2}\right) \gamma\left(\br, \br'\right), \\
V[\gamma]=\int d \br d \br' v\left(\br, \br'\right) \gamma\left(\br', \br\right), \\
N[\gamma]=\int d \br \gamma(\br, \br), \\
S_{\s}[\gamma]=-\sum_i\left[\eta_i \ln \eta_i+\left(1-\eta_i\right) \ln \left(1-\eta_i\right)\right], \\
\Omega_{\H}[\gamma]=\frac{1}{2} \int d \br d \br' w\left(\br, \br'\right) \gamma(\br, \br) \gamma\left(\br', \br'\right), \\
\Omega_{\x}[\gamma]=-\frac{1}{2} \int d \br d \br' w\left(\br, \br'\right) \gamma\left(\br, \br'\right) \gamma\left(\br', \br\right)
\end{array}
\end{equation}
and the form of correlation energy $\Omega_\c[\gamma]$ is unknown.
In general, the external potential could be nonlocal.

Using the spectral representation (Eq. \ref{eq:spetral_representation}), 
we write each term explicitly.

\begin{equation}
\Omega_{k} [\{ \eta_i, \phi_i\}]=   -\half \sum_i \eta_i  \int d\br \phi_i^* (\br) \nabla^2 \phi_i (\br)     
\end{equation}

\begin{equation}
    S_{\s}[\{ \eta_i\}] =   - \sum_i  \left[\eta_i \ln \eta_i+\left(1-\eta_i\right) \ln \left(1-\eta_i\right)\right]
\end{equation}

\begin{equation}
    \begin{aligned}
           &  \Omega_{\H} [\{ \eta_i, \phi_i\}] \\
         = &  \half \int d \br d \br' \sum_i \eta_i   \phi_i^* (\br) \phi_i (\br) \sum_j \eta_j  \phi_j^* (\br')  \phi_j (\br') \\
         = &  \half \int d \br d \br' \sum_{ij} \eta_i \eta_j    \phi_i^* (\br) \phi_i (\br)  \phi_j^* (\br')  \phi_j (\br') \\
    \end{aligned}
\end{equation}

\begin{equation}
    \begin{aligned}
           &  \Omega_{\x} [\{ \eta_i, \phi_i\}] \\
         = &  \half \int d \br d \br' \sum_i \eta_i   \phi_i(\br) \phi_i^*  (\br') \sum_j \eta_j  \phi_j (\br')  \phi_j^*  (\br) \\
         = &  \half \int d \br d \br' \sum_{ij} \eta_i \eta_j    \phi_i (\br) \phi_i^* (\br')  \phi_j (\br')  \phi_j^* (\br) \\
    \end{aligned}
\end{equation}    


\begin{equation}
    N[\{ \eta_i\}] =  \sum_i  \int d \br \eta_i   \phi_i(\br) \phi_i^*  (\br)= \sum_i \eta_i 
\end{equation}
\begin{equation}
    V[\{ \eta_i, \phi_i\}] =  \sum_i  \int d \br  \eta_i  v(\br,\br') \phi_i(\br') \phi_i^*  (\br)
\end{equation}
for local potentials $v(\br, \br') = \delta(\br-\br') v_l(\br)$ it reduces to
\begin{equation}
    V_{l}[\{ \eta_i, \phi_i\}] =  \sum_i  \int d \br  \eta_i  v_l(\br) \phi_i(\br) \phi_i^*  (\br)
\end{equation}

Note that we have included the occupation numbers and natural orbitals explicitly.

The minimization of the grand potential task thus is equivalent to minimize the free energy ${\cal F}[\gamma]$. 
The conservation of the number of electrons and orthonormality of the natural orbitals should be imposed via the Larange multipliers.
Define an auxiliary functional ${\cal L}$,
\begin{eqnarray}
    {\cal L} [\{\eta_i, \phi_i \}] = {\cal F}[\{\eta_i, \phi_i\}] - \sum_{ij} \lambda_{ij} \left[ \braket{\phi_i|\phi_j} - \delta_{ij}\right] - \mu \left[ \sum_i \eta_i - N \right]
\end{eqnarray}
The N-representability requirement $0\leq \eta_i \leq 1$ could be implemented by setting $\eta_i = \cos^{2} \theta_i$.



\begin{equation}
\begin{aligned}
    E\left[\eta_i, \phi_i\right]=& \sum_n \eta_i\left\langle\phi_i\left|-\frac{1}{2} \nabla^2+\hat{V}^{\ext}\right| \phi_i\right\rangle \\
    &+\frac{1}{2} \int d^3 \br d^3 \br' \frac{n(\br) n\left(\br'\right)}{\left|\br-\br'\right|} \\
    &-\half \sum_{j,k=1}^{\infty} f(\eta_j, \eta_k)  \iint d^3r d^3r'  \frac{\phi_j(\br) \phi_j^{*}(\br') \phi_k^{*}(\br) \phi_k(\br')}{|\br-\br'|}
\end{aligned}
\end{equation}
% with fractional occupation numbers $f_i$ obeying the Fermi-Dirac distribution,
% \begin{equation}
%     f_i = \frac{1}{1+ \exp{\frac{\epsilon_}{}}}
% \end{equation}


\bibliography{rdmft}% Produces the bibliography via BibTeX.
\end{document}